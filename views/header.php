<?php
require_once dirname(__DIR__) . "/function/db_function_cart.php";

    if (!empty($_SESSION['user_id'])) {
        $totalQuantity = quantityProductCart($pdo, $_SESSION['user_id']);
        $_SESSION['total_quantity'] = $totalQuantity['total_quantity'];
    }

?>
<!doctype html>
<html class="no-js" lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>EirnnnWorld - ткани ручной покраски </title>
    <meta name="robots" content="noindex, follow"/>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.ico">

    <!-- CSS
	============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Icon Font CSS -->

    <link rel="stylesheet" href="../assets/css/vendor/ionicons.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="../assets/css/plugins/slick.css">
    <link rel="stylesheet" href="../assets/css/plugins/animation.css">
    <link rel="stylesheet" href="../assets/css/plugins/jqueryui.min.css">

    <!-- Main Style CSS (Please use minify version for better website load performance) -->
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/main.css">
    <link rel="stylesheet"  type="text/css" href="../assets/css/my_style.css">
    <!--<link rel="stylesheet" href="assets/css/style.min.css">-->

</head>

<body>

<div class="main-wrapper">

    <header class="fl-header">

        <!-- Header Top Start -->
        <div class="header-top-area d-none d-lg-block">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="header-top-inner">
                            <div class="row">
                                <div class="col-lg-8 col-md-9">
                                    <div class="top-info-wrap text-left">
                                        <ul class="top-info">
                                            <li><a href="#">+380687779767</a></li>
                                            <li><a href="#">eirnnnworld@gmail.com</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-3 text-right">
                                    <div class="social-top">
                                        <ul>
                                            <?php if (empty($_SESSION['user_id'])): ?>
                                                <li><a href="login.php">Войти</a></li>
                                                <li><a href="register.php">Зарегистрироваться</a></li>
                                            <?php else: ?>
                                                <li><a href="/index.php?action=logout" class="btn">Выйти</a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header Top End -->

        <!-- haeader bottom Start -->
        <div class="haeader-bottom-area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-2 col-md-4 col-5">
                        <div class="logo-area">
                            <a href="../index.php"><img src="../assets/images/logo/logo.png" alt="Logo"></a>
                        </div>
                    </div>
                    <div class="col-lg-8 d-none d-lg-block">
                        <div class="main-menu-area text-center">
                            <!--  Start Mainmenu Nav-->
                            <nav class="main-navigation">
                                <ul>
                                    <li class="active"><a href="../index.php">Home</a>
                                    </li>
                                    <li><a href="#">Каталог</a>
                                        <ul class="sub-menu">
                                            <li><a href="shop.html">Anchor</a></li>
                                            <li><a href="shop-right.html">Belfast</a></li>
                                            <li><a href="shop-fullwidth.html">Cashel</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Новости</a>
                                    </li>
                                    <li><a href="about-us.php">О нас</a></li>
                                    <li><a href="contact-us.php">Наши контакты</a></li>
                                </ul>
                            </nav>

                        </div>
                    </div>

                    <div class="col-lg-2 col-md-8 col-7">
                        <div class="right-blok-box d-flex">
                            <div class="search-wrap">
                                <a href="#" class="trigger-search"><i class="ion-ios-search-strong"></i></a>
                            </div>

                            <div class="user-wrap">
                                <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                            </div>


                            <div class="shopping-cart-wrap">
                                <a href="../cart.php"><i class="ion-ios-cart-outline"></i> <span
                                            id="cart-total"><?php echo $_SESSION['total_quantity']; ?></span></a>
                            </div>

                            <div class="mobile-menu-btn d-block d-lg-none">
                                <div class="off-canvas-btn">
                                    <i class="ion-android-menu"></i>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- haeader bottom End -->

        <!-- main-search start -->
        <div class="main-search-active">
            <div class="sidebar-search-icon">
                <button class="search-close"><span class="ion-android-close"></span></button>
            </div>
            <div class="sidebar-search-input">
                <form>
                    <div class="form-search">
                        <input id="search" class="input-text" value="" placeholder="Search entire store here ..."
                               type="search">
                        <button class="search-btn" type="button">
                            <i class="ion-ios-search"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- main-search start -->
        <!-- off-canvas menu start -->
        <aside class="off-canvas-wrapper">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="btn-close-off-canvas">
                    <i class="ion-android-close"></i>
                </div>

                <div class="off-canvas-inner">

                    <!-- mobile menu start -->
                    <div class="mobile-navigation">

                        <!-- mobile menu navigation start -->
                        <nav>
                            <ul class="mobile-menu">
                                <li class="menu-item-has-children"><a href="#">Home</a>
                                    <ul class="dropdown">
                                        <li><a href="index.html">Home version 01</a></li>
                                        <li><a href="index-2.html">Home version 02</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><a href="#">pages</a>
                                    <ul class="megamenu dropdown">
                                        <li class="mega-title has-children"><a href="#">Column One</a>
                                            <ul class="dropdown">
                                                <li><a href="compare.html">Compare Page</a></li>
                                                <li><a href="login-register.html">Login &amp; Register</a></li>
                                                <li><a href="my-account.html">My Account Page</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-title has-children"><a href="#">Column two</a>
                                            <ul class="dropdown">
                                                <li><a href="product-details.html">Product Details 1</a></li>
                                                <li><a href="product-details-2.html">Product Details 2</a></li>
                                                <li><a href="checkout.html">Checkout Page</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-title has-children"><a href="#">Column Three</a>
                                            <ul class="dropdown">
                                                <li><a href="error404.html">Error 404</a></li>
                                                <li><a href="cart.html">Cart Page</a></li>
                                                <li><a href="wishlist.html">Wish List Page</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children "><a href="#">shop</a>
                                    <ul class="dropdown">
                                        <li><a href="shop.html">Shop Left Sidebar</a></li>
                                        <li><a href="shop-right.html">Shop Right Sidebar</a></li>
                                        <li><a href="shop-fullwidth.html">Shop Full Width</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children "><a href="#">Blog</a>
                                    <ul class="dropdown">
                                        <li><a href="blog.html">Blog Left Sidebar</a></li>
                                        <li><a href="blog-right.html">Blog Right Sidebar</a></li>
                                        <li><a href="blog-details.html">Blog Details Page</a></li>
                                    </ul>
                                </li>
                                <li><a href="about-us.html">About</a></li>
                                <li><a href="contact-us.html">Contact</a></li>
                            </ul>
                        </nav>
                        <!-- mobile menu navigation end -->
                    </div>
                    <!-- mobile menu end -->
                    <!-- offcanvas widget area start -->
                    <div class="offcanvas-widget-area">
                        <div class="off-canvas-contact-widget">
                            <ul>
                                <li>
                                    <a href="#">+380687779767</a>
                                </li>
                                <li>
                                    <a href="#">eirnnnworld@gmail.com</a>
                                </li>
                            </ul>
                        </div>
                        <div class="off-canvas-social-widget">
                            <a href="#"><i class="ion-social-facebook"></i></a>
                            <a href="#"><i class="ion-social-instagram"></i></a>
                            <a href="#"><i class="ion-social-instagram"></i></a>
                            <a href="#"><i class="ion-social-googleplus"></i></a>
                        </div>

                    </div>
                    <!-- offcanvas widget area end -->
                </div>
            </div>
        </aside>
        <!-- off-canvas menu end -->
    </header>

