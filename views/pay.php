<?php

require_once ROOT_PATH . "/views/header.php";
$message =  "Ваш заказ принят, с Вами свяжутся в ближайшее время!";

?>
    <div class="container">
        <div class="d-flex justify-content-center pt-50">
            <?php echo $message; ?>
        </div>

    </div>
<?php
require_once ROOT_PATH . "/views/footer.php";
?>
