<?php
require_once ROOT_PATH. "/views/header.php";

?>
<!-- main-content-wrap start -->
<div class="main-content-wrap shop-page section-ptb">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- shop-sidebar-wrap start -->
                <div class="shop-sidebar-wrap">

                    <!-- shop-sidebar start -->
                    <div class="shop-sidebar mb-30">
                        <h4 class="title">Цена</h4>
                        <!-- filter-price-content start -->
                        <div class="filter-price-content">
                            <form action="#" method="post">
                                <div id="price-slider" class="price-slider"></div>
                                <div class="filter-price-wapper">
                                    <div class="filter-price-cont">
                                        <div class="input-type">
                                            <input type="text" id="min_price" readonly="" value="<?php echo $minMaxPrice[0]['min_price'];?>"/>
                                        </div>
                                        <span>—</span>
                                        <div class="input-type">
                                            <input type="text" id="max_price" readonly="" value="<?php echo $minMaxPrice[0]['max_price'];?>"/>
                                        </div>
                                        <br>
                                        <a class="add-to-cart-button pl=5" href="#">
                                            <span>FILTER</span>
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- filter-price-content end -->
                    </div>
                    <!-- shop-sidebar end -->

                    <!-- shop-sidebar start -->
                    <div class="shop-sidebar mb-30">
                        <form action="/index.php" method="POST">
                            <h4>Категории</h4>
                            <?php foreach ($categories as $category): ?>
                                <label><input <?php if (!empty($_POST['categories']) && in_array(
                                            $category['id'],
                                            $_POST['categories']
                                        )) {
                                        echo "checked";
                                    } ?>
                                        class="form-check-input" type="checkbox" name="categories[]"
                                        value="<?php echo $category['id']; ?>"><?php echo $category['category_name']; ?>
                                </label><br>
                            <?php endforeach; ?>
                            <input type="submit" class="btn btn-info" value="Показать"/>
                        </form>
                    </div>
                    <!-- shop-sidebar end -->
                </div>
            </div>
            <!-- output products start -->
            <div class="col-md-9">
                <form method="POST" action="/index.php">
                    <div class="row">
                        <?php foreach ($products as $product): ?>
                            <div class="col-md-4 ">
                                <div class="card mb-4 shadow-sm ">
                                    <?php if(!empty($product['image'])):?>
                                        <img  class="img-fluid product-card-image" src="<?php echo  $product['image'];?>" alt="fabrics">
                                    <?php else:?>
                                        <img class="img-fluid product-card-image" src="<?php echo  PRODUCT_DEFAULT_IMAGE;?>" alt="fabrics">

                                    <?php endif;?>
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between">
                                            <p class="card-text"><?php echo $product['name']; ?></p>
                                           <!-- <p class="card-text"><?php /*echo $product['size']. "см"; */?></p>-->
                                        </div>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group pl-2">
                                                <label class="form-check-label" for="exampleCheck1">В корзину</label>
                                                <input
                                                    class="form-check-input" type="checkbox" name="products[]"
                                                    value="<?php echo $product['id']; ?>">
                                            </div>
                                            <p> <?php echo "\u{20B4}". " " . money_format('%i', $product['price']); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-center ">
                            <input type="submit" class="btn btn-info" value="Заказать"/>
                        </div>
                    </div>
                </form>
                <!-- paginatoin-area start -->
                <div class="paginatoin-area">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 m-auto">
                            <ul class="pagination-box">
                                <li><a class="Previous" href="#"><i class="ion-chevron-left"></i></a>
                                </li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li>
                                    <a class="Next" href="#"><i class="ion-chevron-right"></i> </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- paginatoin-area end -->
            </div>
            <!-- output products end -->

            <!-- shop-product-wrapper end -->
        </div>
    </div>
</div>
<!-- main-content-wrap end -->
<?php
require_once ROOT_PATH. "/views/footer.php";
?>