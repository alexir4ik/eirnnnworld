<?php require_once(ROOT_PATH . "/views/header.php"); ?>

    <!-- main-content-wrap start -->
    <div class="main-content-wrap  section-ptb">
        <div class="container">
            <form class="form-group input-group" method="POST" action="/login.php">
                <fieldset>
                    <div class="row">
                        <div class="form-group input-group ml-auto mr-auto">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                            </div>
                            <div class="col-sm-8">
                                <input class="form-control <?php if (!empty($error['email'])) {
                                    echo 'is-invalid';
                                } ?>" type="email" placeholder="Enter Your Email" required
                                       name="email" id="email" value="<?php
                                $field = $_POST['email'] ?? '';
                                include ROOT_PATH . "/views/form_filling.php";
                                ?>">

                            </div>
                            <div class="col-sm-3">
                                <small id="" class="text-danger">
                                    <?php
                                    $errorField = $error['email'] ?? '';
                                    include ROOT_PATH . "/views/error_validation_message.php";
                                    ?>
                                </small>
                            </div>
                        </div>
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                            </div>
                            <div class="col-sm-8">
                                <input class="form-control <?php if (!empty($error['password'])) {
                                    echo 'is-invalid';
                                } ?>" type="password" placeholder="Create password" required
                                       name="password" id="password">
                            </div>
                            <div class="col-sm-3">
                                <small id="" class="text-danger">
                                    <?php
                                    $errorField = $error['password'] ?? '';
                                    include ROOT_PATH . "/views/error_validation_message.php";
                                    ?>
                                </small>
                            </div>
                        </div>
                        <div class="form-group input-group">
                            <div class="col-sm-8">
                                <input class="btn btn-primary btn-block" type="submit" name="login"
                                       value="Login">
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <!-- main-content-wrap end -->

<?php require_once(ROOT_PATH . "/views/footer.php"); ?>