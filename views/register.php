<?php
require_once ROOT_PATH. "/views/header.php";

?>

    <!-- main-content-wrap start -->
    <div class="main-content-wrap  section-ptb">
        <div class="container">
            <form class="form-group input-group ml-auto mr-auto" method="POST" action="/register.php">
                <fieldset>
                    <div class="row">
                        <div class="form-group input-group">
                            <div class="input-group-prepend"><span class="input-group-text"> <i
                                            class="fa fa-user"></i> </span></div>
                            <div class="col-sm-8">
                                <input class="form-control <?php if (!empty($error['name'])) {
                                    echo 'is-invalid';
                                } ?>" type="text" placeholder="Введите Фамилию и Имя"
                                       name="name" id="name"
                                       value="<?php
                                       $field = $_POST['name'] ?? '';
                                       include ROOT_PATH . "/views/form_filling.php";
                                       ?>">
                            </div>
                            <div class="col-sm-3">
                                <small class="text-danger">
                                    <?php
                                    $errorField = $error['name'] ?? '';
                                    include ROOT_PATH . "/views/error_validation_message.php";
                                    ?>
                                </small>
                            </div>
                        </div>
                        <div class="form-group input-group">
                            <div class="input-group-prepend"><span class="input-group-text"> <i
                                            class="fa fa-key"></i> </span></div>
                            <div class="col-sm-8">
                                <input class="form-control <?php if (!empty($error['name'])) {
                                    echo 'is-invalid';
                                } ?>" type="text" required placeholder="Введите логин"
                                       name="login" id="login" value="<?php
                                $field = $_POST['login'] ?? '';
                                include ROOT_PATH . "/views/form_filling.php";
                                ?>">
                            </div>
                            <div class="col-sm-3">
                                <small class="text-danger">
                                    <?php
                                    $errorField = $error['login'] ?? '';
                                    include ROOT_PATH . "/views/error_validation_message.php";
                                    ?>
                                </small>
                            </div>
                        </div>
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                            </div>
                            <div class="col-sm-8">
                                <input class="form-control <?php if (!empty($error['email'])) {
                                    echo 'is-invalid';
                                } ?>" type="email" placeholder="Введите адрес электронной почты" required
                                       name="email" id="email" value="<?php
                                $field = $_POST['email'] ?? '';
                                include ROOT_PATH . "/views/form_filling.php";
                                ?>">
                            </div>
                            <div class="col-sm-3">
                                <small id="" class="text-danger">
                                    <?php
                                    $errorField = $error['email'] ?? '';
                                    include ROOT_PATH . "/views/error_validation_message.php";
                                    ?>
                                </small>
                            </div>
                        </div>
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                            </div>
                            <div class="col-sm-8">
                                <input class="form-control <?php if (!empty($error['password'])) {
                                    echo 'is-invalid';
                                } ?>" type="password" placeholder="Пароль" required
                                       name="password" id="password">
                            </div>
                            <div class="col-sm-3">
                                <small id="" class="text-danger">
                                    <?php
                                    $errorField = $error['password'] ?? '';
                                    include ROOT_PATH . "/views/error_validation_message.php";
                                    ?>
                                </small>
                            </div>
                        </div>
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                            </div>
                            <div class="col-sm-8">
                                <input class="form-control <?php if (!empty($error['confirm_password'])) {
                                    echo 'is-invalid';
                                } ?>" type="password" placeholder="Подтвердите пароль" required
                                       name="confirm_password" id="confirm_password">
                            </div>
                            <div class="col-sm-3">
                                <small id="" class="text-danger">
                                    <?php
                                    $errorField = $error['confirm_password'] ?? '';
                                    include ROOT_PATH . "/views/error_validation_message.php";
                                    ?>
                                </small>
                            </div>
                        </div>
                        <div class="form-group input-group">
                            <div class="col-sm-8">
                                <input class="btn btn-primary btn-block" type="submit" name="reg_id"
                                       value="Регистрация">
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <!-- main-content-wrap end -->

<?php
require_once ROOT_PATH . "/views/footer.php";
?>
