<?php
require_once ROOT_PATH . "/views/header.php";
?>

    <!-- main-content-wrap start -->
    <div class="main-content-wrap section-ptb cart-page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form action="/cart.php" method="POST" class="cart-table">
                        <div class="table-content table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="plantmore-product-thumbnail">Товар</th>
                                    <th class="cart-product-name">Описание</th>
                                    <th class="plantmore-product-price">Цена за единицу</th>
                                    <th class="plantmore-product-quantity">Количество</th>
                                    <th class="plantmore-product-subtotal">Всего</th>
                                    <th class="plantmore-product-remove">Удалить</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($products as $product): ?>
                                    <tr>
                                        <td class="plantmore-product-thumbnail"><a href="#">
                                                <?php if (!empty($product['image'])): ?>
                                                    <img class="product-card-image"
                                                         src="<?php echo $product['image']; ?>" alt="fabrics">
                                                <?php else: ?>
                                                    <img class="product-card-image"
                                                         src="<?php echo PRODUCT_DEFAULT_IMAGE; ?>" alt="fabrics">
                                                <?php endif; ?>
                                            </a></td>
                                        <td class="plantmore-product-name"><a
                                                    href="#"><?php echo $product['name']; ?></a></td>
                                        <td class="plantmore-product-price"><span
                                                    class="amount"><?php echo "\u{20B4}" . "&nbsp;&nbsp;" . money_format(
                                                        '%i',
                                                        $product['price']
                                                    ); ?></span>
                                        </td>
                                        <td class="plantmore-product-quantity">
                                            <input value="<?php echo $product['selected_quantity']; ?>"
                                                   name="quantity_<?php echo $product['id']; ?>" type="number" min="0"
                                                   max="<?php echo $product['quantity']; ?>">
                                        </td>
                                        <td class="product-subtotal"><span
                                                    class="amount"><?php echo "\u{20B4}" . "&nbsp;&nbsp;" . money_format(
                                                        '%i',
                                                        $product['selected_quantity'] * $product['price']
                                                    ); ?></span></td>
                                        <td class="plantmore-product-remove">

                                            <input type="submit" value="Удалить" id="remove_product"
                                                   name="remove_<?php echo $product['id']; ?>">
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-6 ml-auto ">
                                <div class="cart-page-total">
                                    <ul>
                                        <li><b>Итого к оплате</b>
                                            <span><?php echo "\u{20B4}" . "&nbsp;&nbsp;" . money_format(
                                                        '%i',
                                                        $_SESSION['total_price']
                                                    ); ?></span></li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="row pt-3 d-flex justify-content-between">
                            <div class="col-md-3">
                                <div>
                                    <input class="btn btn-default mb-5" name="update" value="Обновить корзину"
                                           type="submit">
                                    <input type="submit" name="reset" class="btn btn-danger"
                                           value="Очистить корзину"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <button class=" btn-success btn-order" name="order[]" value="products ordered" type="submit">
                                    Оформить заказ
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- main-content-wrap end -->
<?php
require_once ROOT_PATH . "/views/footer.php";
?>