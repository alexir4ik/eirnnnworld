<?php

require_once dirname(__FILE__) . "/config.php";
require_once dirname(__FILE__) . "/function/db_function_user.php";

    $error = [];
    $fieldsMap = [
        "name" => "Фамилия и Имя",
        "login" => "логин",
        "email" => "Адрес электронной почты",
        "password" => "Пароль",
        "confirm_password" => "Confirm Password"
    ];
    if (!empty($_POST)) {
        unset($_POST['reg_id']);
        foreach ($_POST as $k => &$v) {
            $v = trim(strip_tags($v));
            if (empty($v)) {
                $error[$k][] = "Поле " . $fieldsMap[$k] . " не заполнено";
            } else {
                if ($k == "name" && (strlen($v) < 5 || strlen($v) > 150)) {
                    $error[$k][] = "Длина поля " . $fieldsMap[$k] . " должна быть больше 5 и меньше 150 символов";
                }
                if ($k == "login" && (strlen($v) < 5 || strlen($v) > 150)) {
                    $error[$k][] = "Длина поля " . $fieldsMap[$k] . " должна быть больше 5 и меньше 150 символов";
                }
                if ($k == "login" && getUniqueLogin($pdo, $v)) {
                    $error[$k][] = "Данный " . $fieldsMap[$k] . " занят";
                }
                if ($k == "password" && !validatePassword($v)) {
                    $error[$k][] = $fieldsMap[$k] . " не соответствует заданным условиям";
                }
                if ($k == "password" && ($v != trim(strip_tags($_POST['confirm_password'])))) {
                    $error[$k][] = $fieldsMap[$k] . " не подтвержден!";
                }
                if ($k == "email" && !filter_var($v, FILTER_VALIDATE_EMAIL)) {
                    $error[$k][] = $fieldsMap[$k] . " введен неверно!";
                }
                if ($k == "email" && getUniqueEmail($pdo, $v)) {
                    $error[$k][] = "Данный " . $fieldsMap[$k] . " уже используется!";
                }
            }
        }
        unset($v);

        if (empty($error)) {
            $userId = registerUser($pdo, $_POST['name'], $_POST['login'], $_POST['email'], $_POST['password']);
            if (!empty($userId)) {
                $_SESSION['user_id'] = $userId;
                header("Location: /index.php");
                die();
            }
        }
    }


require_once dirname(__FILE__) . "/views/register.php";

