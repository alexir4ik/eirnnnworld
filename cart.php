<?php
require_once dirname(__FILE__) . "/config.php";
require_once dirname(__FILE__) . "/function/db_function_cart.php";


/*    if (!empty($_SESSION['user_id'])) {
        $cartId = getCartId($pdo, $_SESSION['user_id']);
        $_SESSION['cart_id'] = $cartId;
        $_SESSION['products'] = getCartProducts($pdo, $cartId);
        $_SESSION['total_price'] = getTotalPrice($pdo, $_SESSION['cart_id']);
    }*/

    if (empty($_POST) && empty($_SESSION['cart_id'])) {
        header("Location: /index.php");
        exit();
    }

    if (!empty($_POST)) {
        if (!empty($_POST['reset'])) {
            unset($_SESSION['products']);
            deleteAllCartProducts($pdo, $_SESSION['cart_id']);
            updateTotalPrice($pdo, 0, $_SESSION['cart_id']);
            unset($_SESSION['cart_id']);
            header("Location: index.php");
            die();
        }
        unset($_POST['update']);
        foreach ($_POST as $k => $val) {
            $id = explode("_", $k)[1];
            updateCartProductQuantity($pdo, $_SESSION['cart_id'], $id, $val);
            foreach ($_SESSION['products'] as $key => &$product) {
                if (!empty($product) && $product['id'] == $id) {
                    if ($val == 0) {
                        array_splice($_SESSION['products'], $key, 1);
                    } else {
                        $product['quantity'] = $val;
                    }
                }
            }
            unset($product);
        }
        $_SESSION['total_price'] = getTotalPrice($pdo, $_SESSION['cart_id']);
        updateTotalPrice($pdo, $_SESSION['total_price'], $_SESSION['cart_id']);
    }

    $products = getCartProducts($pdo, $_SESSION['cart_id']);


/* $numberOrder = 248;
if (isset($_POST['order'])) {
    $_SESSION['order'] = [];
    $_SESSION['order'] = $products;
    unset($_SESSION['products']);
    header("Location: pay.php");
    exit;
}*/

require_once dirname(__FILE__) . "/views/cart.php";
?>