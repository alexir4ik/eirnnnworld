<?php

    define("ROOT_PATH", dirname(__FILE__));
    define("PRODUCT_DEFAULT_IMAGE", "assets/images/product/unnamed.jpg");
    define("DBNAME", "eirnnnworld");
    define("DBUSER", "db_user");
    define("DBPASS", "1111");
    define("SALT", "5161sggsdgsgs!jrfvgs8");



    $opt = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    ];
    $dsn = "mysql:host=localhost;port=3306;dbname=" . DBNAME . ";charset=utf8";
    $pdo = new PDO($dsn, DBUSER, DBPASS, $opt);
    session_start();
