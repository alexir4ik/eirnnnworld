<?php
require_once dirname(__FILE__) . "/config.php";
require_once dirname(__FILE__) . "/function/db_function_products.php";
require_once dirname(__FILE__) . "/function/db_function_category.php";
require_once dirname(__FILE__) . "/function/db_function_cart.php";
require_once dirname(__FILE__) . "/function/db_function_user.php";

    if (!empty($_GET['action'])) {
        logoutUser();
    }

    if (!empty($_POST['categories'])) {
        $products = getProductsByCategories($pdo, $_POST['categories']);
    } else {
        $products = getAllProducts($pdo);
    }
    $userId = $_SESSION['user_id'] ?? 0;
    if (!empty($_SESSION['user_id'])) {
        $cartId = getCartId($pdo, $_SESSION['user_id']);
        $_SESSION['cart_id'] = $cartId;
        $_SESSION['products'] = getCartProducts($pdo, $cartId);
        $_SESSION['total_price'] = getTotalPrice($pdo, $_SESSION['cart_id']);
    }
    $categories = getALLCategories($pdo);
    // $minMaxPrice = minMaxPrice($pdo);


    if (!empty($_POST['products'])) {
        foreach ($_POST['products'] as $product) {
            $_SESSION['products'][] = ['id' => $product, 'quantity' => 1];
           // $userId = $_SESSION['user_id'] ?? 0;
            if (empty($_SESSION['cart_id'])) {
                $cartId = createCart($pdo, $userId);
                $_SESSION['cart_id'] = $cartId;
            } else {
                $cartId = $_SESSION['cart_id'];
            }
            addProductToCart($pdo, $product, $cartId, 1);

        }
        //update total price
        $_SESSION['total_price'] = getTotalPrice($pdo, $cartId);
        updateTotalPrice($pdo, $_SESSION['total_price'], $cartId);
        //redirect to cart
        header("Location: cart.php");
    }

require_once dirname(__FILE__) . "/views/products.php";

