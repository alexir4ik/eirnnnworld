<?php




function getCartProducts($db, $cartId )
{
    if (empty($db) || empty($cartId)) {
        return [];
    }
    $productsCart = [];
    $stmt = $db->prepare("
            SELECT
                `products`.`id`,
                `products`.`name`,
                `products`.`price`,
                `products`.`image`,
                `cart_products`.`quantity` as selected_quantity,
                `products`.`quantity`
            FROM
                `products`
            INNER JOIN `cart_products` ON `cart_products`.`product_id` = `products`.`id`
            WHERE
                `cart_products`.`cart_id` = :cart_id
            AND `cart_products`.`is_deleted` <> 1"
    );
    $stmt->execute(["cart_id" => $cartId]);
    return $stmt->fetchAll();
}



    function minMaxPrice($db)
    {
        $stmt = $db->prepare(
            "SELECT
                    MIN( `price` ) AS min_price,
                    MAX( `price` ) AS max_price 
                FROM
                    `products`"
        );
        $stmt->execute();
        return $stmt->fetchAll();
    }














?>