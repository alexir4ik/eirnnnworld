<?php
require_once dirname(__FILE__). "/config.php";
require_once dirname(__FILE__) . "/function/db_function_user.php";


    $error = [];
    if (!empty($_POST)) {
        unset($_POST['login']);
        foreach ($_POST as $k => $v) {
            $v = trim(strip_tags($v));
            if (empty($v)) {
                $error[$k][] = "Field " . ucfirst(str_replace("_", " ", $k)) . " should be filled!";
            }
        }
        $idPassword = loginUser($pdo, $_POST['email']);
        if ($k == "password" && (sha1($_POST['password'] . SALT)) !== $idPassword['password']) {
            $error[$k][] = "Password is incorrect!";
            $countIncorrectLogin = getIncorrectLoginItems($pdo, $_POST['email']);
            if ($countIncorrectLogin == 2) {
                lockedStatus($pdo, $_POST['email'], $countIncorrectLogin + 1);
            } else {
                statusUser($pdo, $_POST['email'], $countIncorrectLogin + 1);
            }
        } else {
            {
                if (empty($error)) {
                    $userId = $idPassword['id'];
                    statusUser($pdo, $_POST['email'], 0);
                    if (!empty($userId)) {
                        $_SESSION['user_id'] = $userId;
                        header("Location: /index.php");
                        die();
                    } else {
                        $error["email"][] = "Login or Password is incorrect!";
                    }
                }
            }
        }
    }

require_once dirname(__FILE__) . "/views/login.php";
?>
