<?php
    function getAllProducts($db)
{
    $stmt = $db->prepare("SELECT * FROM `products`");
    $stmt->execute();
    return $stmt->fetchAll();
}