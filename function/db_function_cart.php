<?php
function createCart($db, $userId, $totalPrice = 0)
{

    $stmt = $db->prepare(
        "INSERT INTO `cart` ( `user_id` , `total_price` )
         VALUES
            (
            :user_id,
            :total_price            
            )"
    );
    $stmt->execute(["user_id" => $userId, "total_price" => $totalPrice]);
    return $db->lastInsertId();
}

function addProductToCart($db, $productId, $cartId, $quantity)
{
    if(!getProductFromCart($db, $cartId, $productId)){
        $stmt = $db->prepare(
            "INSERT INTO `cart_products` ( `product_id`, `cart_id`, `quantity` )
             VALUES
                (
                :product_id,
                :cart_id,
                :quantity
                )"
        );
        $stmt->execute(["product_id" => $productId, "cart_id" => $cartId, "quantity" => $quantity]);
        return true;
    }
}

    function updateCartProductQuantity($db, $cartId, $productId, $quantity)
    {
        if (empty($db) || empty($cartId) || empty($productId)) {
            return false;
        }
        if($quantity == 0){
            $stmt = $db->prepare("
                    UPDATE
                        `cart_products`
                    SET
                        `is_deleted` = 1
                    WHERE
                        `product_id` = :product_id
                    AND `cart_id` = :cart_id"
            );
            $stmt->execute(["product_id" => $productId, "cart_id" => $cartId]);
        } else{
            $stmt = $db->prepare("
                    UPDATE
                        `cart_products`
                    SET
                        `quantity` = :quantity
                    WHERE
                        `product_id` = :product_id
                    AND `cart_id` = :cart_id"
            );
            $stmt->execute(["product_id" => $productId, "cart_id" => $cartId, "quantity" => $quantity]);
        }
    }

    function deleteAllCartProducts($db, $cartId)
    {
        if (empty($db) || empty($cartId)) {
            return false;
        }
        $stmt = $db->prepare("
                        UPDATE
                            `cart_products`
                        SET
                            `is_deleted` = 1
                        WHERE
                            `cart_id` = :cart_id"
        );
        $stmt->execute(["cart_id" => $cartId]);
    }

    function getProductFromCart($db, $cartId, $productId )
    {
        if(empty($cartId) || empty($productId)){
            return false;
        }
        $stmt = $db->prepare("
                SELECT 
                    `id`
                FROM
                    `cart_products` 
                WHERE
                    `cart_id` = :cart_id 
                AND
                    `product_id` = :product_id
                AND `is_deleted` = 0");
        $stmt->execute(["cart_id" => $cartId, "product_id" => $productId ]);
        return $stmt->fetch(PDO::FETCH_COLUMN);
    }

    function getCartProducts($db, $cartId )
    {
        if (empty($db) || empty($cartId)) {
            return [];
        }
        $productsCart = [];
        $stmt = $db->prepare("
                SELECT
                    `products`.`id`,
                    `products`.`name`,
                    `products`.`price`,
                    `products`.`image`,
                    `cart_products`.`quantity` as selected_quantity,
                    `products`.`quantity`
                FROM
                    `products`
                INNER JOIN `cart_products` ON `cart_products`.`product_id` = `products`.`id`
                WHERE
                    `cart_products`.`cart_id` = :cart_id
                AND `cart_products`.`is_deleted` <> 1"
        );
        $stmt->execute(["cart_id" => $cartId]);
        return $stmt->fetchAll();
    }

    function getCartId($db, $userId)
    {
        if (empty($db) || empty($userId)) {
            return [];
        }
        $productsCart = [];
        $stmt = $db->prepare("
                SELECT
                    `id` 
                FROM
                    `cart` 
                WHERE
                    `user_id` = :user_id"
        );
        $stmt->execute(["user_id" => $userId]);
        $cartId = $stmt->fetch();
        return $cartId['id'];
    }

    function getTotalPrice($db, $cartId)
    {
        $cartProduct=[];
        $totalPrice = 0;
        $stmt=$db->prepare("
            SELECT
                SUM( `products`.`price` * `cart_products`.`quantity` ) AS total_price 
            FROM
                products
                INNER JOIN cart_products ON `cart_products`.`product_id` = `products`.`id` 
            WHERE
                `cart_products`.`cart_id` = :cart_id 
                AND `cart_products`.`is_deleted` <> 1"
        );
        $stmt->execute(["cart_id" => $cartId]);
        $totalPrice = $stmt->fetch();
        return $totalPrice['total_price'];
    }

    function updateTotalPrice($db, $totalPrice, $cartId)
    { $stmt=$db->prepare("
        UPDATE `cart` 
        SET total_price = :total_price 
        WHERE
            id = :cart_id");
        $stmt->execute(["total_price" => $totalPrice, "cart_id" => $cartId]);
    }

    function quantityProductCart($db, $userId)
    {
        $stmt=$db->prepare("
            SELECT
                SUM( `cart_products`.`quantity` ) AS total_quantity 
            FROM
                `cart_products`
                INNER JOIN `cart` ON `cart`.`id` = `cart_products`.`cart_id` 
            WHERE
                `cart`.`user_id` = :user_id 
                AND `cart_products`.`is_deleted` <> 1");
        $stmt->execute(["user_id" => $userId]);
        return $stmt-> fetch();
    }