<?php
    function registerUser($db, $userName, $login, $email, $password)
    {
        if (empty($db) || empty($userName) || empty($login) || empty($email) || empty($password)) {
            return false;
        }
        $stmt = $db->prepare(
            "
                INSERT INTO `users` (
                    `name`,
                    `login`,
                    `email`,
                    `password`
                )
                VALUES
                    (
                        :name,
                        :login,
                        :email,
                        :password
                    )"
        );

        $stmt->execute(
            [
                "name" => $userName,
                "login" => $login,
                "email" => $email,
                "password" => sha1($password . SALT)
            ]
        );

        return $db->lastInsertId();
    }

    function loginUser($db,  $email)
    {
        if (empty($db) ||  empty($email) ) {
            return false;
        }
        $stmt = $db->prepare("
            SELECT
                `id`,
                `password` 
            FROM
                `users` 
            WHERE
                `email` = :email"
        );
        $stmt->execute(["email" => $email]);
        $idPassword = $stmt->fetch();
        return $idPassword;
    }

    function logoutUser()
    {
        if (!empty($_GET['action'] && $_GET['action'] == "logout")) {
            unset($_SESSION['user_id']);
            session_destroy();
            header("Location: ../index.php");
            exit();
        }
    }

    function getUniqueEmail($db, $email)
    {
        if (empty($db) || empty($email)) {
            return false;
        }
        $stmt = $db->prepare("
            SELECT
                `email` 
            FROM
                `users` 
            WHERE
                `email` = :email"
        );
        $stmt->execute(["email" => $email]);
        return $stmt->fetch();
    }

    function getUniqueLogin($db, $login)
    {
        if (empty($db) || empty($login)) {
            return false;
        }
        $stmt = $db->prepare("
            SELECT
                `login` 
            FROM
                `users` 
            WHERE
                `login` = :login"
        );
        $stmt->execute(["login" => $login]);
        return $stmt->fetch();
    }

    function validatePassword($password)
    {
        if (empty($password) || strlen($password) < 8) {
            return false;
        }
        if (preg_match('/[a-zа-я]+/', $password)) {
            if(preg_match('/[A-ZА-Я]+/', $password)){
                if (preg_match('/[0-9]+/', $password)) {
                    return true;
                } // слово содержит как минимум 1 буквуб 1 заглавную букву и цифру
                else {
                    return false; // не содежржит цифру
                }
            }else{
                return false; //не содежит заглавную букву
            }

        } // слово содержит только 1 букву
        else {
            return false;
        } // слово не содержит ни букв ни цифр
    }

    function checkPasswordIncorrect($db, $email, $password)
    {
        if (empty($db) || empty($email) || empty($password)) {
            return false;
        }
        $stmt = $db->prepare(
            "
        UPDATE `users` 
            SET `incorrect_login_items` = `incorrect_login_items` + 1 
            WHERE
                `email` = :email 
            AND `password` <> :password"
        );
        $stmt->execute(["email" => $email, "password" => sha1($password . SALT)]);
        return $stmt->rowCount();
    }

    function getIncorrectLoginItems( $db, $email)
    {
        $stmt = $db->prepare("
        SELECT
            `incorrect_login_items` 
        FROM
            `users` 
        WHERE
            `email` = :email");
        $stmt->execute(["email" => $email]);
        $incorrectLoginItems = $stmt->fetch();
        return $incorrectLoginItems['incorrect_login_items'];
    }

    function lockedStatus($db, $email, $count)
    {
        $stmt = $db->prepare("
        UPDATE `users` 
        SET `status` = 'Locked',  `incorrect_login_items` = :count 
        WHERE
            `email` = :email");
        $stmt->execute(["email" => $email, "count" => $count ]);
        return "Пользователь заблокирован";
    }

    function statusUser($db, $email, $count)
    {
        $stmt = $db->prepare("
            UPDATE `users` 
            SET `incorrect_login_items` = :count 
            WHERE
                `email` = :email");
        $stmt->execute(["email" => $email, "count" => $count ]);
        return true;
    }
