<?php
    function getALLCategories($db)
    {
        $stmt = $db->prepare("SELECT * FROM `category`");
        $stmt->execute();
        return $stmt->fetchAll();
    }

    function getProductsByCategory($db, $categoryId)
    {
        $stmt = $db->prepare("SELECT * FROM `products` WHERE category_id = :category_id");
        $stmt->execute(["category_id" => $categoryId]);
        return $stmt->fetchAll();
    }

    function getProductsByCategories($db, $categories)
    {
        $in = str_repeat('?,', count($categories) - 1) . '?';
        $stmt = $db->prepare("SELECT * FROM `products` WHERE category_id IN (" . $in . ")");
        $stmt->execute($categories);
        return $stmt->fetchAll();
    }